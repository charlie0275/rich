certifi==2018.4.16
chardet==3.0.4
Django
idna==2.7
locust
pendulum==2.0.3
requests==2.19.1
urllib3==1.23
