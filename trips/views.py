from django.shortcuts import render
from django.views.generic import TemplateView
from common import util
import requests
import collections
import constants

class ForeignNetBuyView(TemplateView):
  def get(self, request, **kwargs):
    response = requests.get(constants.GLOBAL_CONSTANTS['SERVER_URI']+'fetch-foreign-data')
    resp = response.json()
    itemList = resp['netBuyItems']

    viewsUtil = ViewsUtil()
    rankList, accuList = viewsUtil.getRankList(itemList)
    commonList = viewsUtil.getMatchForeAndInve(resp['lastFore'], resp['lastInve'])

    dateUtil = util.DateUtil()
    yesterday = dateUtil.getYesterday()

    d_str = itemList[0].get('date')
    yesterday = dateUtil.stringToDate(d_str)

    return render(request, 'foreign_net_buy.html', {
      'ranks': rankList,
      'accuList' : accuList,
      'commonList': commonList,
      'items': itemList,
      'yesterday': yesterday
    })

class InvestmentNetBuyView(TemplateView):
  def get(self, request, **kwargs):
    response = requests.get(constants.GLOBAL_CONSTANTS['SERVER_URI']+'fetch-investment-data')
    resp = response.json()
    itemList = resp['netBuyItems']

    viewsUtil = ViewsUtil()
    rankList, accuList = viewsUtil.getRankList(itemList)
    commonList = viewsUtil.getMatchForeAndInve(resp['lastFore'], resp['lastInve'])

    dateUtil = util.DateUtil()
    yesterday = dateUtil.getYesterday()

    d_str = itemList[0].get('date')
    yesterday = dateUtil.stringToDate(d_str)

    return render(request, 'investment_net_buy.html', {
      'ranks': rankList,
      'accuList' : accuList,
      'commonList': commonList,
      'items': itemList,
      'yesterday': yesterday
    })

class ForeignNetBuyViewV2(TemplateView):
  def get(self, request, **kwargs):
    response = requests.get(constants.GLOBAL_CONSTANTS['SERVER_URI']+'fetch-foreign-data')
    resp = response.json()
    itemList = resp['netBuyItems']

    viewsUtil = ViewsUtil()
    rankList, accuList = viewsUtil.getRankList(itemList)
    commonList = viewsUtil.getMatchForeAndInve(resp['lastFore'], resp['lastInve'])

    dateUtil = util.DateUtil()
    yesterday = dateUtil.getYesterday()

    d_str = itemList[0].get('date')
    yesterday = dateUtil.stringToDate(d_str)

    return render(request, 'v2/foreign_net_buy.html', {
      'ranks': rankList,
      'accuList' : accuList,
      'commonList': commonList,
      'items': itemList,
      'yesterday': yesterday
    })

class InvestmentNetBuyViewV2(TemplateView):
  def get(self, request, **kwargs):
    response = requests.get(constants.GLOBAL_CONSTANTS['SERVER_URI']+'fetch-investment-data')
    resp = response.json()
    itemList = resp['netBuyItems']

    viewsUtil = ViewsUtil()
    rankList, accuList = viewsUtil.getRankList(itemList)
    commonList = viewsUtil.getMatchForeAndInve(resp['lastFore'], resp['lastInve'])

    dateUtil = util.DateUtil()
    yesterday = dateUtil.getYesterday()

    d_str = itemList[0].get('date')
    yesterday = dateUtil.stringToDate(d_str)

    return render(request, 'v2/investment_net_buy.html', {
      'ranks': rankList,
      'accuList' : accuList,
      'commonList': commonList,
      'items': itemList,
      'yesterday': yesterday
    })

class ViewsUtil:
  def getRankList(self, itemList):
    chineseDict = {}
    timesDict = {}

    accumulateFlag = 1
    accumulateCountDict = {}
    for idx, it in enumerate(itemList):
      if it.get('stock_no_1') not in chineseDict:
        chineseDict[it.get('stock_no_1')] = it.get('stock_name_1')
      if it.get('stock_no_2') not in chineseDict:
        chineseDict[it.get('stock_no_2')] = it.get('stock_name_2')
      if it.get('stock_no_3') not in chineseDict:
        chineseDict[it.get('stock_no_3')] = it.get('stock_name_3')
      if it.get('stock_no_4') not in chineseDict:
        chineseDict[it.get('stock_no_4')] = it.get('stock_name_4')
      if it.get('stock_no_5') not in chineseDict:
        chineseDict[it.get('stock_no_5')] = it.get('stock_name_5')
      if it.get('stock_no_6') not in chineseDict:
        chineseDict[it.get('stock_no_6')] = it.get('stock_name_6')
      if it.get('stock_no_7') not in chineseDict:
        chineseDict[it.get('stock_no_7')] = it.get('stock_name_7')
      if it.get('stock_no_8') not in chineseDict:
        chineseDict[it.get('stock_no_8')] = it.get('stock_name_8')
      if it.get('stock_no_9') not in chineseDict:
        chineseDict[it.get('stock_no_9')] = it.get('stock_name_9')
      if it.get('stock_no_10') not in chineseDict:
        chineseDict[it.get('stock_no_10')] = it.get('stock_name_10')
      if it.get('stock_no_11') not in chineseDict:
        chineseDict[it.get('stock_no_11')] = it.get('stock_name_11')
      if it.get('stock_no_12') not in chineseDict:
        chineseDict[it.get('stock_no_12')] = it.get('stock_name_12')
      if it.get('stock_no_13') not in chineseDict:
        chineseDict[it.get('stock_no_13')] = it.get('stock_name_13')
      if it.get('stock_no_14') not in chineseDict:
        chineseDict[it.get('stock_no_14')] = it.get('stock_name_14')
      if it.get('stock_no_15') not in chineseDict:
        chineseDict[it.get('stock_no_15')] = it.get('stock_name_15')
      if it.get('stock_no_16') not in chineseDict:
        chineseDict[it.get('stock_no_16')] = it.get('stock_name_16')
      if it.get('stock_no_17') not in chineseDict:
        chineseDict[it.get('stock_no_17')] = it.get('stock_name_17')
      if it.get('stock_no_18') not in chineseDict:
        chineseDict[it.get('stock_no_18')] = it.get('stock_name_18')
      if it.get('stock_no_19') not in chineseDict:
        chineseDict[it.get('stock_no_19')] = it.get('stock_name_19')
      if it.get('stock_no_20') not in chineseDict:
        chineseDict[it.get('stock_no_20')] = it.get('stock_name_20')
      if it.get('stock_no_21') not in chineseDict:
        chineseDict[it.get('stock_no_21')] = it.get('stock_name_21')
      if it.get('stock_no_22') not in chineseDict:
        chineseDict[it.get('stock_no_22')] = it.get('stock_name_22')
      if it.get('stock_no_23') not in chineseDict:
        chineseDict[it.get('stock_no_23')] = it.get('stock_name_23')
      if it.get('stock_no_24') not in chineseDict:
        chineseDict[it.get('stock_no_24')] = it.get('stock_name_24')
      if it.get('stock_no_25') not in chineseDict:
        chineseDict[it.get('stock_no_25')] = it.get('stock_name_25')
      if it.get('stock_no_26') not in chineseDict:
        chineseDict[it.get('stock_no_26')] = it.get('stock_name_26')
      if it.get('stock_no_27') not in chineseDict:
        chineseDict[it.get('stock_no_27')] = it.get('stock_name_27')
      if it.get('stock_no_28') not in chineseDict:
        chineseDict[it.get('stock_no_28')] = it.get('stock_name_28')
      if it.get('stock_no_29') not in chineseDict:
        chineseDict[it.get('stock_no_29')] = it.get('stock_name_29')
      if it.get('stock_no_30') not in chineseDict:
        chineseDict[it.get('stock_no_30')] = it.get('stock_name_30')
      if it.get('stock_no_31') not in chineseDict:
        chineseDict[it.get('stock_no_31')] = it.get('stock_name_31')
      if it.get('stock_no_32') not in chineseDict:
        chineseDict[it.get('stock_no_32')] = it.get('stock_name_32')
      if it.get('stock_no_33') not in chineseDict:
        chineseDict[it.get('stock_no_33')] = it.get('stock_name_33')
      if it.get('stock_no_34') not in chineseDict:
        chineseDict[it.get('stock_no_34')] = it.get('stock_name_34')
      if it.get('stock_no_35') not in chineseDict:
        chineseDict[it.get('stock_no_35')] = it.get('stock_name_35')
      if it.get('stock_no_36') not in chineseDict:
        chineseDict[it.get('stock_no_36')] = it.get('stock_name_36')
      if it.get('stock_no_37') not in chineseDict:
        chineseDict[it.get('stock_no_37')] = it.get('stock_name_37')
      if it.get('stock_no_38') not in chineseDict:
        chineseDict[it.get('stock_no_38')] = it.get('stock_name_38')
      if it.get('stock_no_39') not in chineseDict:
        chineseDict[it.get('stock_no_39')] = it.get('stock_name_39')
      if it.get('stock_no_40') not in chineseDict:
        chineseDict[it.get('stock_no_40')] = it.get('stock_name_40')


      if it.get('stock_no_1') not in timesDict:
        timesDict[it.get('stock_no_1')] = 1
      else:
        timesDict[it.get('stock_no_1')] = timesDict[it.get('stock_no_1')] + 1
      if it.get('stock_no_2') not in timesDict:
        timesDict[it.get('stock_no_2')] = 1
      else:
        timesDict[it.get('stock_no_2')] = timesDict[it.get('stock_no_2')] + 1
      if it.get('stock_no_3') not in timesDict:
        timesDict[it.get('stock_no_3')] = 1
      else:
        timesDict[it.get('stock_no_3')] = timesDict[it.get('stock_no_3')] + 1
      if it.get('stock_no_4') not in timesDict:
        timesDict[it.get('stock_no_4')] = 1
      else:
        timesDict[it.get('stock_no_4')] = timesDict[it.get('stock_no_4')] + 1
      if it.get('stock_no_5') not in timesDict:
        timesDict[it.get('stock_no_5')] = 1
      else:
        timesDict[it.get('stock_no_5')] = timesDict[it.get('stock_no_5')] + 1
      if it.get('stock_no_6') not in timesDict:
        timesDict[it.get('stock_no_6')] = 1
      else:
        timesDict[it.get('stock_no_6')] = timesDict[it.get('stock_no_6')] + 1
      if it.get('stock_no_7') not in timesDict:
        timesDict[it.get('stock_no_7')] = 1
      else:
        timesDict[it.get('stock_no_7')] = timesDict[it.get('stock_no_7')] + 1
      if it.get('stock_no_8') not in timesDict:
        timesDict[it.get('stock_no_8')] = 1
      else:
        timesDict[it.get('stock_no_8')] = timesDict[it.get('stock_no_8')] + 1
      if it.get('stock_no_9') not in timesDict:
        timesDict[it.get('stock_no_9')] = 1
      else:
        timesDict[it.get('stock_no_9')] = timesDict[it.get('stock_no_9')] + 1
      if it.get('stock_no_10') not in timesDict:
        timesDict[it.get('stock_no_10')] = 1
      else:
        timesDict[it.get('stock_no_10')] = timesDict[it.get('stock_no_10')] + 1
      if it.get('stock_no_11') not in timesDict:
        timesDict[it.get('stock_no_11')] = 1
      else:
        timesDict[it.get('stock_no_11')] = timesDict[it.get('stock_no_11')] + 1
      if it.get('stock_no_12') not in timesDict:
        timesDict[it.get('stock_no_12')] = 1
      else:
        timesDict[it.get('stock_no_12')] = timesDict[it.get('stock_no_12')] + 1
      if it.get('stock_no_13') not in timesDict:
        timesDict[it.get('stock_no_13')] = 1
      else:
        timesDict[it.get('stock_no_13')] = timesDict[it.get('stock_no_13')] + 1
      if it.get('stock_no_14') not in timesDict:
        timesDict[it.get('stock_no_14')] = 1
      else:
        timesDict[it.get('stock_no_14')] = timesDict[it.get('stock_no_14')] + 1
      if it.get('stock_no_15') not in timesDict:
        timesDict[it.get('stock_no_15')] = 1
      else:
        timesDict[it.get('stock_no_15')] = timesDict[it.get('stock_no_15')] + 1
      if it.get('stock_no_16') not in timesDict:
        timesDict[it.get('stock_no_16')] = 1
      else:
        timesDict[it.get('stock_no_16')] = timesDict[it.get('stock_no_16')] + 1
      if it.get('stock_no_17') not in timesDict:
        timesDict[it.get('stock_no_17')] = 1
      else:
        timesDict[it.get('stock_no_17')] = timesDict[it.get('stock_no_17')] + 1
      if it.get('stock_no_18') not in timesDict:
        timesDict[it.get('stock_no_18')] = 1
      else:
        timesDict[it.get('stock_no_18')] = timesDict[it.get('stock_no_18')] + 1
      if it.get('stock_no_19') not in timesDict:
        timesDict[it.get('stock_no_19')] = 1
      else:
        timesDict[it.get('stock_no_19')] = timesDict[it.get('stock_no_19')] + 1
      if it.get('stock_no_20') not in timesDict:
        timesDict[it.get('stock_no_20')] = 1
      else:
        timesDict[it.get('stock_no_20')] = timesDict[it.get('stock_no_20')] + 1
      if it.get('stock_no_21') not in timesDict:
        timesDict[it.get('stock_no_21')] = 1
      else:
        timesDict[it.get('stock_no_21')] = timesDict[it.get('stock_no_21')] + 1
      if it.get('stock_no_22') not in timesDict:
        timesDict[it.get('stock_no_22')] = 1
      else:
        timesDict[it.get('stock_no_22')] = timesDict[it.get('stock_no_22')] + 1
      if it.get('stock_no_23') not in timesDict:
        timesDict[it.get('stock_no_23')] = 1
      else:
        timesDict[it.get('stock_no_23')] = timesDict[it.get('stock_no_23')] + 1
      if it.get('stock_no_24') not in timesDict:
        timesDict[it.get('stock_no_24')] = 1
      else:
        timesDict[it.get('stock_no_24')] = timesDict[it.get('stock_no_24')] + 1
      if it.get('stock_no_25') not in timesDict:
        timesDict[it.get('stock_no_25')] = 1
      else:
        timesDict[it.get('stock_no_25')] = timesDict[it.get('stock_no_25')] + 1
      if it.get('stock_no_26') not in timesDict:
        timesDict[it.get('stock_no_26')] = 1
      else:
        timesDict[it.get('stock_no_26')] = timesDict[it.get('stock_no_26')] + 1
      if it.get('stock_no_27') not in timesDict:
        timesDict[it.get('stock_no_27')] = 1
      else:
        timesDict[it.get('stock_no_27')] = timesDict[it.get('stock_no_27')] + 1
      if it.get('stock_no_28') not in timesDict:
        timesDict[it.get('stock_no_28')] = 1
      else:
        timesDict[it.get('stock_no_28')] = timesDict[it.get('stock_no_28')] + 1
      if it.get('stock_no_29') not in timesDict:
        timesDict[it.get('stock_no_29')] = 1
      else:
        timesDict[it.get('stock_no_29')] = timesDict[it.get('stock_no_29')] + 1
      if it.get('stock_no_30') not in timesDict:
        timesDict[it.get('stock_no_30')] = 1
      else:
        timesDict[it.get('stock_no_30')] = timesDict[it.get('stock_no_30')] + 1
      if it.get('stock_no_31') not in timesDict:
        timesDict[it.get('stock_no_31')] = 1
      else:
        timesDict[it.get('stock_no_31')] = timesDict[it.get('stock_no_31')] + 1
      if it.get('stock_no_32') not in timesDict:
        timesDict[it.get('stock_no_32')] = 1
      else:
        timesDict[it.get('stock_no_32')] = timesDict[it.get('stock_no_32')] + 1
      if it.get('stock_no_33') not in timesDict:
        timesDict[it.get('stock_no_33')] = 1
      else:
        timesDict[it.get('stock_no_33')] = timesDict[it.get('stock_no_33')] + 1
      if it.get('stock_no_34') not in timesDict:
        timesDict[it.get('stock_no_34')] = 1
      else:
        timesDict[it.get('stock_no_34')] = timesDict[it.get('stock_no_34')] + 1
      if it.get('stock_no_35') not in timesDict:
        timesDict[it.get('stock_no_35')] = 1
      else:
        timesDict[it.get('stock_no_35')] = timesDict[it.get('stock_no_35')] + 1
      if it.get('stock_no_36') not in timesDict:
        timesDict[it.get('stock_no_36')] = 1
      else:
        timesDict[it.get('stock_no_36')] = timesDict[it.get('stock_no_36')] + 1
      if it.get('stock_no_37') not in timesDict:
        timesDict[it.get('stock_no_37')] = 1
      else:
        timesDict[it.get('stock_no_37')] = timesDict[it.get('stock_no_37')] + 1
      if it.get('stock_no_38') not in timesDict:
        timesDict[it.get('stock_no_38')] = 1
      else:
        timesDict[it.get('stock_no_38')] = timesDict[it.get('stock_no_38')] + 1
      if it.get('stock_no_39') not in timesDict:
        timesDict[it.get('stock_no_39')] = 1
      else:
        timesDict[it.get('stock_no_39')] = timesDict[it.get('stock_no_39')] + 1
      if it.get('stock_no_40') not in timesDict:
        timesDict[it.get('stock_no_40')] = 1
      else:
        timesDict[it.get('stock_no_40')] = timesDict[it.get('stock_no_40')] + 1
  
      if idx == 0:
        accumulateCountDict[it.get('stock_no_1')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_2')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_3')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_4')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_5')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_6')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_7')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_8')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_9')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_10')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_11')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_12')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_13')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_14')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_15')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_16')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_17')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_18')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_19')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_20')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_21')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_22')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_23')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_24')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_25')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_26')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_27')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_28')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_29')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_30')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_31')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_32')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_33')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_34')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_35')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_36')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_37')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_38')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_39')] = accumulateFlag
        accumulateCountDict[it.get('stock_no_40')] = accumulateFlag

      elif idx < 5:
        if it.get('stock_no_1') in accumulateCountDict and accumulateCountDict[it.get('stock_no_1')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_1')] = accumulateFlag
        if it.get('stock_no_2') in accumulateCountDict and accumulateCountDict[it.get('stock_no_2')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_2')] = accumulateFlag
        if it.get('stock_no_3') in accumulateCountDict and accumulateCountDict[it.get('stock_no_3')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_3')] = accumulateFlag
        if it.get('stock_no_4') in accumulateCountDict and accumulateCountDict[it.get('stock_no_4')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_4')] = accumulateFlag
        if it.get('stock_no_5') in accumulateCountDict and accumulateCountDict[it.get('stock_no_5')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_5')] = accumulateFlag
        if it.get('stock_no_6') in accumulateCountDict and accumulateCountDict[it.get('stock_no_6')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_6')] = accumulateFlag
        if it.get('stock_no_7') in accumulateCountDict and accumulateCountDict[it.get('stock_no_7')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_7')] = accumulateFlag
        if it.get('stock_no_8') in accumulateCountDict and accumulateCountDict[it.get('stock_no_8')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_8')] = accumulateFlag
        if it.get('stock_no_9') in accumulateCountDict and accumulateCountDict[it.get('stock_no_9')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_9')] = accumulateFlag
        if it.get('stock_no_10') in accumulateCountDict and accumulateCountDict[it.get('stock_no_10')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_10')] = accumulateFlag
        if it.get('stock_no_11') in accumulateCountDict and accumulateCountDict[it.get('stock_no_11')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_11')] = accumulateFlag
        if it.get('stock_no_12') in accumulateCountDict and accumulateCountDict[it.get('stock_no_12')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_12')] = accumulateFlag
        if it.get('stock_no_13') in accumulateCountDict and accumulateCountDict[it.get('stock_no_13')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_13')] = accumulateFlag
        if it.get('stock_no_14') in accumulateCountDict and accumulateCountDict[it.get('stock_no_14')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_14')] = accumulateFlag
        if it.get('stock_no_15') in accumulateCountDict and accumulateCountDict[it.get('stock_no_15')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_15')] = accumulateFlag
        if it.get('stock_no_16') in accumulateCountDict and accumulateCountDict[it.get('stock_no_16')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_16')] = accumulateFlag
        if it.get('stock_no_17') in accumulateCountDict and accumulateCountDict[it.get('stock_no_17')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_17')] = accumulateFlag
        if it.get('stock_no_18') in accumulateCountDict and accumulateCountDict[it.get('stock_no_18')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_18')] = accumulateFlag
        if it.get('stock_no_19') in accumulateCountDict and accumulateCountDict[it.get('stock_no_19')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_19')] = accumulateFlag
        if it.get('stock_no_20') in accumulateCountDict and accumulateCountDict[it.get('stock_no_20')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_20')] = accumulateFlag
        if it.get('stock_no_21') in accumulateCountDict and accumulateCountDict[it.get('stock_no_21')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_21')] = accumulateFlag
        if it.get('stock_no_22') in accumulateCountDict and accumulateCountDict[it.get('stock_no_22')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_22')] = accumulateFlag
        if it.get('stock_no_23') in accumulateCountDict and accumulateCountDict[it.get('stock_no_23')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_23')] = accumulateFlag
        if it.get('stock_no_24') in accumulateCountDict and accumulateCountDict[it.get('stock_no_24')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_24')] = accumulateFlag
        if it.get('stock_no_25') in accumulateCountDict and accumulateCountDict[it.get('stock_no_25')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_25')] = accumulateFlag
        if it.get('stock_no_26') in accumulateCountDict and accumulateCountDict[it.get('stock_no_26')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_26')] = accumulateFlag
        if it.get('stock_no_27') in accumulateCountDict and accumulateCountDict[it.get('stock_no_27')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_27')] = accumulateFlag
        if it.get('stock_no_28') in accumulateCountDict and accumulateCountDict[it.get('stock_no_28')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_28')] = accumulateFlag
        if it.get('stock_no_29') in accumulateCountDict and accumulateCountDict[it.get('stock_no_29')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_29')] = accumulateFlag
        if it.get('stock_no_30') in accumulateCountDict and accumulateCountDict[it.get('stock_no_30')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_30')] = accumulateFlag
        if it.get('stock_no_31') in accumulateCountDict and accumulateCountDict[it.get('stock_no_31')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_31')] = accumulateFlag
        if it.get('stock_no_32') in accumulateCountDict and accumulateCountDict[it.get('stock_no_32')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_32')] = accumulateFlag
        if it.get('stock_no_33') in accumulateCountDict and accumulateCountDict[it.get('stock_no_33')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_33')] = accumulateFlag
        if it.get('stock_no_34') in accumulateCountDict and accumulateCountDict[it.get('stock_no_34')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_34')] = accumulateFlag
        if it.get('stock_no_35') in accumulateCountDict and accumulateCountDict[it.get('stock_no_35')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_35')] = accumulateFlag
        if it.get('stock_no_36') in accumulateCountDict and accumulateCountDict[it.get('stock_no_36')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_36')] = accumulateFlag
        if it.get('stock_no_37') in accumulateCountDict and accumulateCountDict[it.get('stock_no_37')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_37')] = accumulateFlag
        if it.get('stock_no_38') in accumulateCountDict and accumulateCountDict[it.get('stock_no_38')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_38')] = accumulateFlag
        if it.get('stock_no_39') in accumulateCountDict and accumulateCountDict[it.get('stock_no_39')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_39')] = accumulateFlag
        if it.get('stock_no_40') in accumulateCountDict and accumulateCountDict[it.get('stock_no_40')] == accumulateFlag -1:
          accumulateCountDict[it.get('stock_no_40')] = accumulateFlag
      accumulateFlag += 1

    if None in timesDict:
      del timesDict[None]
    if None in accumulateCountDict:
      del accumulateCountDict[None]

    #print(chineseDict)
    #print(timesDict)
    orderedTimesDict = collections.OrderedDict(sorted(timesDict.items(), key=lambda t: t[1], reverse=True))
    #print(orderedTimesDict)
    #print(orderedTimesDict.keys())
    #print(accumulateCountDict)

    class RankItem:
      no = ""
      name = ""
      times = 0
      def __init__(self, no, name, times):
        self.no = no
        self.name = name
        self.times = times
      def __iter__(self):
        return iter([('no',self.no),('name',self.name),('times',self.times)])

    keylist = []
    keylist.extend(iter(orderedTimesDict.keys()))
    rank_1_key = keylist[0]
    rank_2_key = keylist[1]
    rank_3_key = keylist[2]
    rank_4_key = keylist[3]
    rank_5_key = keylist[4]

    finalRankList = [dict(RankItem(rank_1_key, chineseDict[rank_1_key], timesDict[rank_1_key])), 
      dict(RankItem(rank_2_key, chineseDict[rank_2_key], timesDict[rank_2_key])),
      dict(RankItem(rank_3_key, chineseDict[rank_3_key], timesDict[rank_3_key])),
      dict(RankItem(rank_4_key, chineseDict[rank_4_key], timesDict[rank_4_key])),
      dict(RankItem(rank_5_key, chineseDict[rank_5_key], timesDict[rank_5_key]))]

    finalAccuList = []
    for stock_no in accumulateCountDict.keys():
      if accumulateCountDict[stock_no] == 5:
        obj = {"stock_no":stock_no,"stock_name":chineseDict[stock_no],"accumulate_count":accumulateCountDict[stock_no]}
        finalAccuList.append(dict(obj))

    orderedFinalAccuList = sorted(finalAccuList, key=lambda x: x['accumulate_count'], reverse=True)

    return finalRankList, orderedFinalAccuList

  def getMatchForeAndInve(self, lastFore, lastInve):
    inveChineseDict = {}

    inveAmountDict = {}
    foreAmountDict = {}

    inveChineseDict[lastInve.get('stock_no_1')] = lastInve.get('stock_name_1')
    inveChineseDict[lastInve.get('stock_no_2')] = lastInve.get('stock_name_2')
    inveChineseDict[lastInve.get('stock_no_3')] = lastInve.get('stock_name_3')
    inveChineseDict[lastInve.get('stock_no_4')] = lastInve.get('stock_name_4')
    inveChineseDict[lastInve.get('stock_no_5')] = lastInve.get('stock_name_5')
    inveChineseDict[lastInve.get('stock_no_6')] = lastInve.get('stock_name_6')
    inveChineseDict[lastInve.get('stock_no_7')] = lastInve.get('stock_name_7')
    inveChineseDict[lastInve.get('stock_no_8')] = lastInve.get('stock_name_8')
    inveChineseDict[lastInve.get('stock_no_9')] = lastInve.get('stock_name_9')
    inveChineseDict[lastInve.get('stock_no_10')] = lastInve.get('stock_name_10')
    inveChineseDict[lastInve.get('stock_no_11')] = lastInve.get('stock_name_11')
    inveChineseDict[lastInve.get('stock_no_12')] = lastInve.get('stock_name_12')
    inveChineseDict[lastInve.get('stock_no_13')] = lastInve.get('stock_name_13')
    inveChineseDict[lastInve.get('stock_no_14')] = lastInve.get('stock_name_14')
    inveChineseDict[lastInve.get('stock_no_15')] = lastInve.get('stock_name_15')
    inveChineseDict[lastInve.get('stock_no_16')] = lastInve.get('stock_name_16')
    inveChineseDict[lastInve.get('stock_no_17')] = lastInve.get('stock_name_17')
    inveChineseDict[lastInve.get('stock_no_18')] = lastInve.get('stock_name_18')
    inveChineseDict[lastInve.get('stock_no_19')] = lastInve.get('stock_name_19')
    inveChineseDict[lastInve.get('stock_no_20')] = lastInve.get('stock_name_20')
    inveChineseDict[lastInve.get('stock_no_21')] = lastInve.get('stock_name_21')
    inveChineseDict[lastInve.get('stock_no_22')] = lastInve.get('stock_name_22')
    inveChineseDict[lastInve.get('stock_no_23')] = lastInve.get('stock_name_23')
    inveChineseDict[lastInve.get('stock_no_24')] = lastInve.get('stock_name_24')
    inveChineseDict[lastInve.get('stock_no_25')] = lastInve.get('stock_name_25')
    inveChineseDict[lastInve.get('stock_no_26')] = lastInve.get('stock_name_26')
    inveChineseDict[lastInve.get('stock_no_27')] = lastInve.get('stock_name_27')
    inveChineseDict[lastInve.get('stock_no_28')] = lastInve.get('stock_name_28')
    inveChineseDict[lastInve.get('stock_no_29')] = lastInve.get('stock_name_29')
    inveChineseDict[lastInve.get('stock_no_30')] = lastInve.get('stock_name_30')
    inveChineseDict[lastInve.get('stock_no_31')] = lastInve.get('stock_name_31')
    inveChineseDict[lastInve.get('stock_no_32')] = lastInve.get('stock_name_32')
    inveChineseDict[lastInve.get('stock_no_33')] = lastInve.get('stock_name_33')
    inveChineseDict[lastInve.get('stock_no_34')] = lastInve.get('stock_name_34')
    inveChineseDict[lastInve.get('stock_no_35')] = lastInve.get('stock_name_35')
    inveChineseDict[lastInve.get('stock_no_36')] = lastInve.get('stock_name_36')
    inveChineseDict[lastInve.get('stock_no_37')] = lastInve.get('stock_name_37')
    inveChineseDict[lastInve.get('stock_no_38')] = lastInve.get('stock_name_38')
    inveChineseDict[lastInve.get('stock_no_39')] = lastInve.get('stock_name_39')
    inveChineseDict[lastInve.get('stock_no_40')] = lastInve.get('stock_name_40')

    inveAmountDict[lastInve.get('stock_no_1')] = lastInve.get('net_buy_1')
    inveAmountDict[lastInve.get('stock_no_2')] = lastInve.get('net_buy_2')
    inveAmountDict[lastInve.get('stock_no_3')] = lastInve.get('net_buy_3')
    inveAmountDict[lastInve.get('stock_no_4')] = lastInve.get('net_buy_4')
    inveAmountDict[lastInve.get('stock_no_5')] = lastInve.get('net_buy_5')
    inveAmountDict[lastInve.get('stock_no_6')] = lastInve.get('net_buy_6')
    inveAmountDict[lastInve.get('stock_no_7')] = lastInve.get('net_buy_7')
    inveAmountDict[lastInve.get('stock_no_8')] = lastInve.get('net_buy_8')
    inveAmountDict[lastInve.get('stock_no_9')] = lastInve.get('net_buy_9')
    inveAmountDict[lastInve.get('stock_no_10')] = lastInve.get('net_buy_10')
    inveAmountDict[lastInve.get('stock_no_11')] = lastInve.get('net_buy_11')
    inveAmountDict[lastInve.get('stock_no_12')] = lastInve.get('net_buy_12')
    inveAmountDict[lastInve.get('stock_no_13')] = lastInve.get('net_buy_13')
    inveAmountDict[lastInve.get('stock_no_14')] = lastInve.get('net_buy_14')
    inveAmountDict[lastInve.get('stock_no_15')] = lastInve.get('net_buy_15')
    inveAmountDict[lastInve.get('stock_no_16')] = lastInve.get('net_buy_16')
    inveAmountDict[lastInve.get('stock_no_17')] = lastInve.get('net_buy_17')
    inveAmountDict[lastInve.get('stock_no_18')] = lastInve.get('net_buy_18')
    inveAmountDict[lastInve.get('stock_no_19')] = lastInve.get('net_buy_19')
    inveAmountDict[lastInve.get('stock_no_20')] = lastInve.get('net_buy_20')
    inveAmountDict[lastInve.get('stock_no_21')] = lastInve.get('net_buy_21')
    inveAmountDict[lastInve.get('stock_no_22')] = lastInve.get('net_buy_22')
    inveAmountDict[lastInve.get('stock_no_23')] = lastInve.get('net_buy_23')
    inveAmountDict[lastInve.get('stock_no_24')] = lastInve.get('net_buy_24')
    inveAmountDict[lastInve.get('stock_no_25')] = lastInve.get('net_buy_25')
    inveAmountDict[lastInve.get('stock_no_26')] = lastInve.get('net_buy_26')
    inveAmountDict[lastInve.get('stock_no_27')] = lastInve.get('net_buy_27')
    inveAmountDict[lastInve.get('stock_no_28')] = lastInve.get('net_buy_28')
    inveAmountDict[lastInve.get('stock_no_29')] = lastInve.get('net_buy_29')
    inveAmountDict[lastInve.get('stock_no_30')] = lastInve.get('net_buy_30')
    inveAmountDict[lastInve.get('stock_no_31')] = lastInve.get('net_buy_31')
    inveAmountDict[lastInve.get('stock_no_32')] = lastInve.get('net_buy_32')
    inveAmountDict[lastInve.get('stock_no_33')] = lastInve.get('net_buy_33')
    inveAmountDict[lastInve.get('stock_no_34')] = lastInve.get('net_buy_34')
    inveAmountDict[lastInve.get('stock_no_35')] = lastInve.get('net_buy_35')
    inveAmountDict[lastInve.get('stock_no_36')] = lastInve.get('net_buy_36')
    inveAmountDict[lastInve.get('stock_no_37')] = lastInve.get('net_buy_37')
    inveAmountDict[lastInve.get('stock_no_38')] = lastInve.get('net_buy_38')
    inveAmountDict[lastInve.get('stock_no_39')] = lastInve.get('net_buy_39')
    inveAmountDict[lastInve.get('stock_no_40')] = lastInve.get('net_buy_40')

    foreAmountDict[lastFore.get('stock_no_1')] = lastFore.get('net_buy_1')
    foreAmountDict[lastFore.get('stock_no_2')] = lastFore.get('net_buy_2')
    foreAmountDict[lastFore.get('stock_no_3')] = lastFore.get('net_buy_3')
    foreAmountDict[lastFore.get('stock_no_4')] = lastFore.get('net_buy_4')
    foreAmountDict[lastFore.get('stock_no_5')] = lastFore.get('net_buy_5')
    foreAmountDict[lastFore.get('stock_no_6')] = lastFore.get('net_buy_6')
    foreAmountDict[lastFore.get('stock_no_7')] = lastFore.get('net_buy_7')
    foreAmountDict[lastFore.get('stock_no_8')] = lastFore.get('net_buy_8')
    foreAmountDict[lastFore.get('stock_no_9')] = lastFore.get('net_buy_9')
    foreAmountDict[lastFore.get('stock_no_10')] = lastFore.get('net_buy_10')
    foreAmountDict[lastFore.get('stock_no_11')] = lastFore.get('net_buy_11')
    foreAmountDict[lastFore.get('stock_no_12')] = lastFore.get('net_buy_12')
    foreAmountDict[lastFore.get('stock_no_13')] = lastFore.get('net_buy_13')
    foreAmountDict[lastFore.get('stock_no_14')] = lastFore.get('net_buy_14')
    foreAmountDict[lastFore.get('stock_no_15')] = lastFore.get('net_buy_15')
    foreAmountDict[lastFore.get('stock_no_16')] = lastFore.get('net_buy_16')
    foreAmountDict[lastFore.get('stock_no_17')] = lastFore.get('net_buy_17')
    foreAmountDict[lastFore.get('stock_no_18')] = lastFore.get('net_buy_18')
    foreAmountDict[lastFore.get('stock_no_19')] = lastFore.get('net_buy_19')
    foreAmountDict[lastFore.get('stock_no_20')] = lastFore.get('net_buy_20')
    foreAmountDict[lastFore.get('stock_no_21')] = lastFore.get('net_buy_21')
    foreAmountDict[lastFore.get('stock_no_22')] = lastFore.get('net_buy_22')
    foreAmountDict[lastFore.get('stock_no_23')] = lastFore.get('net_buy_23')
    foreAmountDict[lastFore.get('stock_no_24')] = lastFore.get('net_buy_24')
    foreAmountDict[lastFore.get('stock_no_25')] = lastFore.get('net_buy_25')
    foreAmountDict[lastFore.get('stock_no_26')] = lastFore.get('net_buy_26')
    foreAmountDict[lastFore.get('stock_no_27')] = lastFore.get('net_buy_27')
    foreAmountDict[lastFore.get('stock_no_28')] = lastFore.get('net_buy_28')
    foreAmountDict[lastFore.get('stock_no_29')] = lastFore.get('net_buy_29')
    foreAmountDict[lastFore.get('stock_no_30')] = lastFore.get('net_buy_30')
    foreAmountDict[lastFore.get('stock_no_31')] = lastFore.get('net_buy_31')
    foreAmountDict[lastFore.get('stock_no_32')] = lastFore.get('net_buy_32')
    foreAmountDict[lastFore.get('stock_no_33')] = lastFore.get('net_buy_33')
    foreAmountDict[lastFore.get('stock_no_34')] = lastFore.get('net_buy_34')
    foreAmountDict[lastFore.get('stock_no_35')] = lastFore.get('net_buy_35')
    foreAmountDict[lastFore.get('stock_no_36')] = lastFore.get('net_buy_36')
    foreAmountDict[lastFore.get('stock_no_37')] = lastFore.get('net_buy_37')
    foreAmountDict[lastFore.get('stock_no_38')] = lastFore.get('net_buy_38')
    foreAmountDict[lastFore.get('stock_no_39')] = lastFore.get('net_buy_39')
    foreAmountDict[lastFore.get('stock_no_40')] = lastFore.get('net_buy_40')

    #print(inveChineseDict)
    #print(inveAmountDict)
    #print(foreAmountDict)

    if None in foreAmountDict:
      del foreAmountDict[None]
    if None in inveAmountDict:
      del inveAmountDict[None]
    foreNoList = foreAmountDict.keys()
    inveNoList = inveAmountDict.keys()
    foreNoSet = set(foreNoList)
    inveNoSet = set(inveNoList)

    matchNoSet = set(foreNoSet) & set(inveNoSet)
    print('common net buy : '+str(matchNoSet))

    finalList = []
    for stock_no in matchNoSet:
      obj = {"stock_no":stock_no,"stock_name":inveChineseDict[stock_no],
      "inve_amount":inveAmountDict[stock_no],"fore_amount":foreAmountDict[stock_no]}
      finalList.append(dict(obj))

    orderedFinalList = sorted(finalList, key=lambda x: x['inve_amount'], reverse=True)
    #print(orderedFinalList)
    return orderedFinalList