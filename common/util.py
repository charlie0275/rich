from datetime import datetime, timedelta
import pendulum

class DateUtil:
  @staticmethod
  def getMinusTimezoneDay(days):
    now = pendulum.now('Asia/Taipei')
    return now.subtract(days=days)

  def getYesterday(self):
    return DateUtil.getMinusTimezoneDay(1)

  def stringToDate(self, d_str):
    return datetime.strptime(d_str, '%Y-%m-%d')