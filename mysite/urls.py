"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.contrib import admin
from django.views.generic.base import RedirectView
from trips import views

urlpatterns = [
	url(r'^$', views.InvestmentNetBuyView.as_view(), name='home'),
    url(r'^inve/$', views.InvestmentNetBuyView.as_view(), name='investment'),
    url(r'^fore/$', views.ForeignNetBuyView.as_view(), name='foreign'),

    url(r'^v2/$', views.InvestmentNetBuyViewV2.as_view(), name='home_v2'),
    url(r'^v2/inve/$', views.InvestmentNetBuyViewV2.as_view(), name='investment_v2'),
    url(r'^v2/fore/$', views.ForeignNetBuyViewV2.as_view(), name='foreign_v2'),

    url(r'^favicon.ico$', RedirectView.as_view(url='/static/img/favicon.ico', permanent=False)),
]
