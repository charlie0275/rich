from locust import HttpLocust, TaskSet, task

class BasicTaskSet(TaskSet):
    def on_start(self):
        """ on_start is called when a Locust start before any task is scheduled """
        self.begin()

    def on_stop(self):
        """ on_stop is called when the TaskSet is stopping """
        self.end()

    def begin(self):
        print("========== basic task set begin ==========")

    def end(self):
        print("========== basic task set end ==========")

    @task(6)
    def TestInvestmentNetBuy(self):
        self.client.get("/")

    @task(2)
    def TestForeignNetBuy(self):
        self.client.get("/fore/")

class BasicLocust(HttpLocust):
    weight = 3
    task_set = BasicTaskSet
    min_wait = 2000
    max_wait = 5000