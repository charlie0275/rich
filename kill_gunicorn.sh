#!/bin/bash
# kill gunicorn processes

PID=`ps aux |grep gunicorn |grep python | awk '{ print $2 }'`
echo "kill gunicorn : $PID"
if ps -p $PID > /dev/null; then
  kill -9 $PID
fi
